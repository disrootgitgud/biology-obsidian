Repeat for all chapters: 
1. Flip Through each page in the chapter (Tells your brain what to look out for, how it's structured, and other subconscious benefits) 
2. Read Questions at end of chapter (Baader-Meinhof Phenomenon) 
3. Read Bold Words in the chapter (Titles, subtitles, Topic-Headings. Get a better understanding of the structure) 
4. Read First and Last Sentence of each paragraph (First sentence gives an indication on what the rest of the paragraph is about, the last sentence sums up the important information) 
5. Read through the chapter and take notes (Brings everything together and lets you connect the dots)