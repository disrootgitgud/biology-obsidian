```mermaid
flowchart TD;
	Large_Biological_Molecules --> Carbohydrates;
	Large_Biological_Molecules --> Lipids;
	Large_Biological_Molecules --> Proteins;
	Large_Biological_Molecules --> Nucleic_Acids;
```

## Carbohydrates

Almost all carbohydrates are **hydrophilic** (“water-­ loving”) molecules that dissolve readily in water.


Carbohydrates are of three types:

```mermaid
flowchart TD;
	Carbohydrates --> Monosaccharides;
	Carbohydrates --> Disaccharides;
	Carbohydrates --> Polysaccharides;
```


### Monosaccharides

^abd861

Simple sugars, or monosaccharides (from the Greek mono, single, and sacchar, sugar), are the monomers of carbohydrates; they cannot be broken down into smaller sugars.

Examples:

- **glucose** found in soft drinks ^3b3c70

- **fructose** found in fruit, sweeter than glucose ^65710a

- **glucose** and **fructose** are also found in honey

Molecular formula of glucose is $$C_6H_{12}O_6$$. Fructose has the same formula, but its atoms are arranged differently. 

Glucose and fructose are examples of [[isomers]]

### Disaccharides
A disaccharide, or double sugar, is constructed from two monosaccharides by a [[dehydration reaction]].

Eg. 
- **lactose** which is made out of the monosaccharides [[2_Large Biological Molecules#^3b3c70 | glucose]] and galactose.
 ^7aad59
- **maltose** found in naturally in germinating seeds. It consists of two molecules of [[2_Large Biological Molecules#^3b3c70 | glucose]] 

- **sucrose** (table sugar), which consists of a [[2_Large Biological Molecules#^3b3c70| glucose]] monomer linked to a [[2_Large Biological Molecules#^65710a | fructose]] monomer. It is found in plant sap.

	HFCS (High Fructose Corn Syrup)

### Polysaccharides

Polysaccharides are long chains of sugar-polymers of [[2_Large Biological Molecules#^abd861 | monosaccharids]]

Eg.
- **starch** consists of long strings of [[2_Large Biological Molecules#^3b3c70 | glucose]] monomers.  Plant cells store starch, providing a sugar stockpile that can be tapped when needed. Potatoes and grains, such as wheat, corn, and rice, are the major sources of starch in our diet. Animals can digest starch because enzymes within their digestive systems break the bonds between glucose monomers through [[hydrolysis]] reactions.
- **glycogen** - Animals store excess glucose in the form of a polysac-
charide called glycogen. Glycogen is a polymer of [[2_Large Biological Molecules#^3b3c70 | glucose]], but glucose is more extensively branched.
- **cellulose** is also a polymer of [[2_Large Biological Molecules#^3b3c70 | glucose]]. cellulose cannot be broken by any enzyme produced by animals. Grazing animals and wood- eating insects such as termites are able to derive nutrition from cellulose because microorganisms inhabiting their digestive tracts break it down. ^834465


>[!Extra Information]-
> The cellulose in plant foods that you eat, commonly known as dietary fiber (roughage) passes through your digestive tract unchanged. Because it remains undigested, fiber does not provide nutrients, but it does help keep>[!Extra Information]-
> The cellulose in plant foods that you eat, commonly known as dietary fiber (roughage) passes through your digestive tract unchanged. Because it remains undi- gested, fiber does not provide nutrients, but it does help keep your digestive system healthy. The passage of cel- lulose stimulates cells lining the digestive tract to secrete mucus, which allows food to pass smoothly. The health benefits of dietary fiber include lowering the risk of heart disease, diabetes, and gastrointestinal disease. your digestive system healthy. The passage of cel- lulose stimulates cells lining the digestive tract to secrete mucus, which allows food to pass smoothly. The health benefits of dietary fiber include lowering the risk of heart disease, diabetes, and gastrointestinal disease.

---

## Lipids

 Lipids are **hydrophobic** (“water-fearing”); they do not mix with water. **Lipids are not polymers.**

Lipids differ from carbohydrates, proteins, and nucleic acids in that they are neither huge macromolecules nor are they necessarily polymers built from repeating monomers. Lipids are a diverse group of molecules made from different molecular building blocks. 


Lipids are of two types: 
```mermaid
flowchart TD;
	Lipids --> Fats;
	Lipids --> Steroids;
```

### Fats
A typical **fat** consists of a **glycerol** molecule joined with three **fatty acid molecules** by [[dehydration reaction]]. The resulting fat molecule is called a **triglyceride**.

A **fatty acid** is a long molecule that stores a lot of energy.

>[!Question]- Where are fats stored?
>We stock these long-term food stores in specialized reservoirs called **adipose cells** which swell and shrink when we deposit and withdraw fat from them. This adipose tissue, or body fat, not only stores energy but also cushions vital organs and insulates us, helping maintain a constant, warm body temperature.

>[!Question]- What are the uses of fats?
> - energy storage
> - cushioning
> - insulation

#### Types of fatty acids

```mermaid
flowchart TD;
	Fatty_Acids --> Saturated_Fatty_Acids;
	Fatty_Acids --> Unsaturated_Fatty_Acids;
```

#TODO 2nd Last paragraph of page 77

- Most animal fats, such as **lard** and **butter** have a high proportion of saturated fatty acids. The linear shape of saturated fatty acids allows these molecules to stack easily (like bricks in a wall), so saturated fats tend to be **solid** at room temperature. Diets rich in saturated fats may contribute to cardiovascular disease by promoting [[atherosclerosis]].
- Most unsaturated fats are **liquid** at room temperature. Fats that are primarily unsaturated include **vegetable oils** (such as **corn and canola oil**) and **fish oils** (such as **cod liver oil**)

Saturated fats can be converted to unsaturated fats by adding  hydrogen by a process called **hydrogenation**.  ^03aee0

**[[2_Large Biological Molecules#^03aee0 | Hydrogenation]]**
- adds hydrogen, 
- converts unsaturated fats to saturated fats, 
- makes liquid fats solid at room temperature, 
- creates trans fats, a type of unsaturated fat that is particularly bad for your health. 


**Trans Fat** should be generally avoided and saturated fat should be limited. Fats containing **omega-3 fatty acids** have been shown to reduce the risk of heart disease and relieve the symptoms of arthritis and inflammatory bowel disease. Some sources of these beneficial fats are nuts and oily fish such as salmon.

### Steriods

^717f50

All steriods have a carbon skeleton with four fused rings.

![[steroids.png]]

Eg:
- **Chlorestrol**: assosciated with cardiovascular diseases. 

  However, it is a key component of the **cell membrane**.
  
  It is also the “base steroid” from which your body produces other steroids, such as the hormones estrogen and testosterone ­ (Figure 3.13), which are responsible for the development of female and male sex characteristics, respectively.
  
  It is a type of **sterol** with four fused rings in its molecular structure.



**Anabolic Steriods** are
- synthetic variants of testosterone.
- mimic some of testosterone's effects 
- maybe prescribed to treat diseases such as cancer and AIDS (diseases which cause muscle wasting).

---

## Proteins 
A protein is a polymer of amino acid monomers. Proteins account for more than 50% of the dry weight of most cells. Proteins are the “worker bees” of your body. It is instrumental in almost everything a cell does.

Eg: 
- **Hemoglobin**: found within red blood cells, helps in transporting oxygen. ^ba5bfd
 - **Keratin** : found in hair and nails

### The Monomers of Proteins: Amino Acids
All proteins are made by stringing together a common set of **20** kinds of **amino acids**. 
#### Structure of an amino acid
Every amino acid consists of a **central carbon atom** bonded to four covalent partners. Three of those attachments are common to all 20 amino acids: 
- a carboxyl group (—COOH) 
- an amino group (—NH2)
- hydrogen atom. 

The variable component of amino acids is called the side chain (or R group, for radical group); it is attached to the fourth bond of the central carbon. Each type of amino acid has a unique side chain which gives that amino acid its special chemical properties. 

Some amino acids have very simple side chains; the amino acid glycine, for example, has a single hydrogen as its side chain.


![[amino_acids.png]]

![[alanine.jpg]]
Alanine amino acid (* diagram given in book is wrong )

### Protein Shape

Cells link amino acids monomers together by [[dehydration reaction]]. The bond that joins adjacent amino acids is called a **peptide bond**. 

The resulting long chain of amino acids is called a **polypeptide**. ^fbda79

The amino acid sequence of each [[2_Large Biological Molecules#^fbda79 | polypeptide]] determines the three-dimensional structure of the protein. And it is a protein’s three-dimensional structure that enables the molecule to carry out its specific function. 

Nearly all proteins work by recognizing and binding to some other molecule. For example, the specific shape of lactase enables it to recognize and attach to [[2_Large Biological Molecules#^7aad59 | lactose]], its molecular target. For all proteins, structure and function are interrelated.

Even a slight change in the amino acid sequence can affect a protein’s ability to function. 
Eg: 
- The substitution of one amino acid for another in [[2_Large Biological Molecules#^ba5bfd | hemoglobin]] causes sickle cell disease, an inherited blood disorder.
- Misfolded proteins are associated with several severe brain disorders. the diseases shown in [[brain_disease.png | figure 3.20]] are all caused by **prions**, misfolded versions of normal brain proteins. **Prions** can infiltrate the brain, converting normally folded proteins into the abnormal shape. Clustering of the misfolded proteins eventually disrupts brain function. 

![[brain_disease.png]]

A protein’s shape is sensitive to the environment. An unfavourable change  in temperature, pH or some other factor can cause a protein to unravel.

---

## Nucleic Acids 
Nucleic acids are macromolecules that store information and provide the instructions for building proteins. The name nucleic comes from the fact that DNA is found in the nuclei of [[eukaryotic cells]].

There are actually two types of nucleic acids:

```mermaid
flowchart  TD;
	Nucleic_Acids --> DNA_deoxyribonucleic_acids;
	Nucleic_Acids --> RNA_ribonucleic_acids;
```

The genetic material that humans and all other organisms inherit from their parents consists of giant molecules of DNA. The DNA resides in the cell as one or more very long fibers called **chromosomes**

A gene is a unit of inheritance encoded in a specific stretch of DNA that programs the amino acid sequence of a **[[2_Large Biological Molecules#^fbda79 | polypeptide]]**. Those programmed instructions, however, are written in a chemical code that must be translated from “nucleic acid language” to “protein language” . A cell’s **RNA** molecules help make this translation.

**Nucleic Acids** are polymers made from monomers called **nucleotides**. ^7cc0ee

Each nucleotide contains three parts. At the center of each nucleotide is a five-carbon sugar (blue in the figure), deoxyribose in DNA and ribose in RNA. Attached to the sugar is a negatively charged phosphate group (yellow) containing a phosphorus atom bonded to oxygen atoms (PO4−). Also attached to the sugar is a nitrogen-containing base (green) made of one or two rings. The sugar and phosphate are the same in all nucleotides; only the base varies. Each DNA nucleotide has one of four possible nitrogenous bases: 
- adenine (A) ^0f30e8
- guanine (G)
- cytosine (C)
- thymine (T)

![[dna_nucleotide.png]]

[[dehydration reaction | Dehydration reactions]] link [[2_Large Biological Molecules#^7cc0ee | nucleotide ]] monomers into long chains called **polynucleotides**. ^a943e1

![[polynucleotide.png]]
In a [[2_Large Biological Molecules#^a943e1 | polynucleotide]],  [[2_Large Biological Molecules#^7cc0ee | nucleotides ]] are joined by covalent bonds between the sugar of one nucleotide and the phosphate of the next. This bonding results in a sugar-phosphate backbone, a repeating pattern of sugar-phosphate-sugar-phosphate, with the bases (A, T, C, or G) hanging off the backbone like appendages.
![[dna_double_helix.png]]
A molecule of cellular DNA is double-stranded, with
two [[2_Large Biological Molecules#^a943e1 | polynucleotide]] strands coiled around each other to
form a **double helix**.

**The base A can pair only with T, and G can pair only
with C.** ([[2_Large Biological Molecules#^0f30e8 | Full forms of ATGC]])
Thus, if you know the sequence of bases along one DNA strand, you also know the sequence along the complementary strand in the double helix.

#### Similarities between DNA and RNA

- both are polymers of [[2_Large Biological Molecules#^7cc0ee | nucleotides]] made up of a sugar, a phosphate and a base.

#### Differences between DNA and RNA 

- As its name ribonucleic acid denotes, its sugar is **ribose** rather than **deoxyribose**. ^d5e129
- Instead of the base thymine, RNA has a similar but distinct base called ­**uracil** (U).
  
  Except for the presence of [[2_Large Biological Molecules#^d5e129 | ribose]] and [[2_Large Biological Molecules#^f7ef0d| uracil]], an RNA [[2_Large Biological Molecules#^a943e1 | polynucleotide]] chain is identical to a DNA [[2_Large Biological Molecules#^a943e1 | polynucleotide]] chain

- RNA is usually found in living cells in single-stranded form, whereas DNA usually exists in a double helix form.