Methane is abundant in natural gas and is also produced by ![[prokaryotes]] that live in swamps (in the form of swamp gas) and in the digestive tracts of grazing animals, such as cows.

Larger molecules are called ![[macromolecules]]. Molecules are **polymers**, large molecules made by stringing together many smaller molecules called **monomers**.

Cells link monomers together to form a polymer through a [[dehydration reaction]].The breakdown of polymers occurs by a process called [[hydrolysis]]

---
**Next**: [[2_Large Biological Molecules]]