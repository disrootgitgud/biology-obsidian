Cells are of two types:

```mermaid
	flowchart TD;
	Cells --> Prokaroytic_Cell
	Cells --> Eukaryotic_Cell
```

Prokaryotic Cells are found in organisms of the domains **Backeteria** and **Archea**, known as **prokaryotes**. ^d5ffda

![[prokaryotic_cell.png]]
^^ A ­drawing of an idealized [[1_Two Categories of the Cell#^d5ffda | prokaryotic ]] cell ^^

Organisms of the domain **Eukarya** - including **protists**, **plants**, **fungi** and **animals** are composed of **Eukaryotic Cells**


## Similarities  between Prokaryotic and Eukaryotic Cells

- both have a **plasma membrane** which regulates the traffic of molecules between the cell and its surroundings.
- Inside both kinds of cells, there is a thick, jelly-like fluid called the **cytosol**, in which cellular componets are present. ^558751
- All cells have one or more chromosomes ­carrying genes made of DNA.
- All cells have **ribosomes** that build proteins according to the instructions from the **genes**.

## Differences between Prokaryotic and Eukaryotic Cells

| Prokaryotic Cell                                                                      | Eukaryotic Cell                |
| ------------------------------------------------------------------------------------- | ------------------------------ |
| appeared more than 3.5 billion years ago                                              | appeared 2.1 billion years ago |
| Are usually much smaller, one-tenth the size of eukaryotic cell, simpler in structure | Larger and more complex        |
| do not have [[organelles]]                                                             | have [[organelles]]             |
| Do not have a nucleus                                                                 | have a nucleus                 |

^772abb

Most prokaryotic cells have a rigid cell wall surrounding their plasma membrane.

>[!Question]- What is the function of the rigid cell wall
> - protects the cell 
> - helps maintain the shape

In some prokaryotes, a sticky outer coat called a **capsule** surrounds the cell wall.


>[!Question]- What is the function of the capsule
> - provides protection 
> - helps [[1_Two Categories of the Cell#^d5ffda | prokaryotes ]] stick to the surface surfaces and other cells in a colony. Eg. capsules help bacteria in your mouth stick together to form harmful dental plaque.

Prokaryotes can also have **pili** which can also attach to surfaces.

----



cytosol