The region of the cell outside the nucleus and within the plasma membrane is called the **cytoplasm**. The **cytoplasm**  of a eukaryotic cell consists of various **organelles** [[1_Two Categories of the Cell#^772abb | organelles ]] suspended in the liquid [[1_Two Categories of the Cell#^558751 | cytosol]]. ^9a0d7a

## Difference between plant cells and animal cells 

plant cells have 
- chloroplasts 
- central vacuole
- a cell wall 

animal cells have 
- centrioles
- [[2_An Overview of Eukaryotic Cells#^a46be4 | lysosomes]]

# Membrane Structure 

## The Plasma Membrane 

The **plasma membrane** and other membranes are composed mostly of **phospholipids**. ^339bad

 Each **phospholipid** is composed of two distinct regions— a **"head"** with a **negatively charged phosphate group** and **two nonpolar fatty acid “tails”**. **Phospholipids** group ­together to form a ­two-layer sheet called a **phospholipid­ ­bilayer**. ^9e41cb

![[phospholipid bilayer.png]]
The phospholipids’ ­hydrophilic (“water-loving”) heads are arranged to face outward, ­exposed to the aqueous solutions on both sides of a membrane. Their hydro- phobic ­(“water-fearing”) tails are arranged inward, mingling with each other and shielded from water.

![[fluid_mosaic_model_of_membrane.png]]
Suspended in the [[2_An Overview of Eukaryotic Cells#^9e41cb | phospholipid ]] ­bilayer of most membranes are proteins that **help regulate traffic across the membrane**; these proteins also ­perform other functions.

>[!Extra Information ]-
>Membranes are not static sheets of molecules locked rigidly in place, however. In fact, the texture of a cellular membrane is similar to salad oil. The phospholipids and most of the proteins can therefore drift about within the membrane. Thus, a membrane is a fluid mosaic—fluid because the molecules can move freely past one another and mosaic because of the diversity of proteins that float like icebergs in the phospholipid sea.

## Cell Surfaces 

### Plant cells

Plant cells have a **cell wall** surrounding their [[2_An Overview of Eukaryotic Cells#^339bad | cell membrane]] made from **[[2_Large Biological Molecules#^834465 | cellulose]]**.

>[!Question] What are the functions of the cell walls 
> - to protect the cells 
> - maintain cell shape 
> - keep cells from absorbing so much water that they burst

Plant cells are connected to each other via channels that pass through the cell walls, joining the cytoplasm of each cell to that of its neighbors. These channels allow water and other small molecules to move between cells, integrating the activities of a tissue.

### Animal cells 

Most animal cells secrete a sticky coat called the **extracellular matrix**. 

**Fibers** made of the protein **collagen** (also found in skin, cartilage, bones, and tendons) **hold cells together in tissues** and can also have protective and supportive functions. 

In addition, the surfaces of most animal cells contain cell junctions, structures that connect cells together into tissues, allowing the cells to function in a coordinated way.

# Nucleus 

^9fbdc1

The **nucleus** is separated from the [[#^9a0d7a | cytoplasm]] by a double membrane called the **nuclear envelope**. Each membrane of the nuclear envelope is similar to the [[#^339bad | structure of the plasma membrane]]. ^1b64df

![[nucleus.png]]

Pores in the envelope allow certain material to pass between the nucleus and the  [[#^9a0d7a | cytoplasm]].

 Within the nucleus, long DNA molecules and associated proteins form fibers called **chromatin**. Each long **chromatin fiber** constitutes one **chromosome**.

 The **nucleolus** ,a prominent ­structure within the **nucleus**, **is the site where the ­components of ribosomes are made**.

>[!Question]- Where are components of ribosomes made?
> In the nucleolus 

# Ribosomes 

^95ba1b

>[!Question] What is the function of ribosomes?
>Ribosomes are **responsible for protein synthesis**.

>[!Question] Where are the ribosomes made?
In eukaryotic cells, the components of ribosomes are made in the [[#^9fbdc1 | nucleus]] and then transported through the pores of the [[2_An Overview of Eukaryotic Cells#^1b64df | nuclear envelope]] into the [[2_An Overview of Eukaryotic Cells#^9a0d7a | cytoplasm]]. In the cytoplasm the ribosomes begin their work.

>[!Question] Where are ribosomes found in the cell?
Some ribosomes are suspended in the [[2_An Overview of Eukaryotic Cells#^9a0d7a | cytosol]], making proteins that remain within the fluid of the cell. Other ribosomes are attached to the outside of the nucleus or an organelle called the endoplasmic reticulum, making proteins that are incorporated into membranes or secreted by the cell.

Free and bound ribosomes are structurally identical, and ribosomes can switch locations, moving between the endoplasmic reticulum and the [[2_An Overview of Eukaryotic Cells#^9a0d7a | cytosol]].

# The Endomembrane System: Manufacturing and Distributing Cellular Products

## The Endoplasmic Reticulum 

It is connected to the [[2_An Overview of Eukaryotic Cells#^1b64df | nuclear envelope]].

![[ER.png]]

There are two components that make up the Endoplasmic Reticulum(ER):

```mermaid
flowchart TD;
ER --> Rough_ER
ER --> Smooth_ER
```

### Rough Endoplasmic Reticulum 

The “rough” in rough ER refers to ribosomes that stud the outside of its membrane. **One of the functions of rough ER is to make more membrane.** Phospholipids made by enzymes of the rough ER are inserted into the ER membrane. In this way, the ER membrane grows, and portions of it can bubble off and be transferred to other parts of the cell.

![[ER_manufacture.png]] ^429964

### Smooth Endoplasmic Reticulum 

The “smooth” in smooth ER refers to the fact that this organelle lacks the [[2_An Overview of Eukaryotic Cells#^95ba1b | ribosomes]] that populate the ­surface of rough ER.

Function of Smooth ER is the synthesis of lipids, including [[2_Large Biological Molecules#^717f50 | steroids]].

## Golgi Apparatus 

It receives, refines, stores and distributes chemical products of the cell. 

Products made in the ER reach the golgi apparatus in [[2_An Overview of Eukaryotic Cells#^429964 | transport vesicles]].

- One side of a Golgi stack serves as a receiving dock for vesicles from the ER.
- Proteins within a vesicle are usually modified by enzymes ­during their transit from the receiving to the shipping side of the Golgi apparatus.
- The shipping side of a Golgi stack is a depot from which finished ­products can be carried in transport vesicles to other [[organelles]] or to the plasma membrane.

­Vesicles that bind with the plasma membrane transfer proteins to it or secrete ­finished products to the outside of the cell.

## Lysosomes 

#TODO 

## Vacuoles

#TODO 

# Energy Transformations: Chloroplasts and Mitochondria

## Chloroplasts 


