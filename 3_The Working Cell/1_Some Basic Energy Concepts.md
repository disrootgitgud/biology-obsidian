# Conservation  of Energy 

**Energy** is defined as the capacity to cause change.

**Conservation of ­energy** explains that it is not possible to destroy or create energy. Energy can only be converted from one form to another. 

**potential energy** the energy an object has ­because of its location or structure.

# Heat 

The energy has been converted to **heat**, a type of kinetic energy contained in the random motion of atoms and molecules. Heat is energy in its most disordered, chaotic form, the energy of aim- less molecular movement.

**Entropy** is a measure of the amount of disorder, or randomness, in a system. Every time energy is converted from one form to an- other, entropy increases.

# Chemical Energy 


# Food Calories

A calorie (cal) is the amount of energy that can raise the temperature of 1 gram (g) of water by 1°C.

it’s conventional to use kilocalories (kcal), units of 1,000 calories. In fact, the Calories (capital C) on a food package are actually kilocalories.

---

**Next:** [[2_Energy Transformations ATP and Cellular Work]]