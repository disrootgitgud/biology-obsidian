>[!Extra Information ]-
>The carbohydrates, fats, and other fuel molecules we obtain from food cannot be used directly as fuel for our cells. Instead, the chemical energy released by the breakdown of organic molecules during cellular respiration is used to generate molecules of ATP. These molecules of ATP then power cellular work. ATP acts like an energy shuttle, storing energy obtained from food and then releasing it as needed at a later time.

# The structure of ATP

^31ba08

The abbreviation of ATP stands for **adenosine triphosphate**.

ATP consists of an organic molecule called adenosine plus a tail of three phosphate groups. 

The triphosphate tail is the part that provides energy for cellular work. Each phosphate group is negatively charged. Negative charges repel each other. The crowding of negative charges in the triphosphate tail contributes to the potential energy of ATP. It is the release of the phosphate at the tip of the triphosphate tail that makes energy available to working cells. What remains is **ADP**, **adenosine diphosphate** (two phosphate groups instead of three). ^5c44b1


![[ATP_power.png]]

# Phosphate Transfer 

When ATP drives work in [[1_Two Categories of the Cell | cells]] by being converted to ADP, the released phosphate groups don’t just fly off into space. ATP energizes other molecules in [[2_An Overview of Eukaryotic Cells | cells]] by transferring phosphate groups to those molecules. When a target molecule accepts the third phosphate group, it becomes energized and can then perform work in the cell.

# The ATP Cycle 

Cells spend [[2_Energy Transformations ATP and Cellular Work#^31ba08 | ATP]] continuously. Fortunately, it is a renewable resource. ATP can be restored by adding a phosphate group back to [[#^5c44b1 | ADP]]. That takes energy, like recompressing a spring. And that’s where food enters the picture. The chemical energy that cellular ­respiration harvests from sugars and other organic fuels is put to work regenerating a cell’s supply of ATP. Cellular work spends ATP, which is recycled when ADP and phosphate are combined using energy ­released by cellular respiration.

![[ATP_cycle.png]]

---

**Previous:** [[1_Some Basic Energy Concepts]]
**Next:** [[3_Enzymes]]